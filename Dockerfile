FROM php:5.6-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    libzip-dev \
    zip \
    unzip 

# Install NVM and set to LTS
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
# RUN nvm install --lts
# RUN nvm install node
# RUN nvm use node
RUN apt install -y nodejs

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd gettext xml intl zip ext-gmp
 
 #Enable php extension
 RUN docker-php-ext-enable mbstring exif pcntl bcmath gd gettext xml intl zip ext-gmp
# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

# Set working directory
WORKDIR /var/www

USER $user
